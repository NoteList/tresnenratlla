package tresenRatlla.vista;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Arrays.asList;

public class SocketService {
    private static Socket socket;
    public static void connectToRobot() {
        try{

            socket = new Socket ("172.23.5.203", 50000);

            DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
            dOut.writeUTF("Estic Dintre");
            dOut.flush();

        }
        catch(IOException e){

        }

    }
    public static void sendScript(String script){
        StringBuffer instr = new StringBuffer();
        try{
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
            out.println(script);
            out.flush();
            InputStreamReader isr = new InputStreamReader(bis, StandardCharsets.US_ASCII);

        }
        catch(Exception e){
            System.err.println(e);
        }
    }
    public static void closeConnectionToRobot(){
        try{
            socket.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void printCommonNames(Collection<String> commons, int width, int columnCount) {
        int column = 0;
        String format = "%-" + width + "s";
        for (String name : commons) {
            System.out.printf(format, name);
            column = ++column % columnCount;
            if (column == 0) System.out.println();
        }
    }
}

